#ifndef TABLE_H
#define TABLE_H

//#include <QString>
//#include <QSqlQueryModel>
#include "customsqlmodel.h"
//#include "vsqlquerymodel.h"
#include "settings.h"

class Table
{
public:
  Table();
  ~Table();
  bool openTable();
  bool reopenTable();
  TableSettings tableSettings;

protected:
  friend class TableIOMapper;
  CustomSqlModel * m_model;
  //VSqlQueryModel * m_model;
  //QSqlQueryModel * m_model;
};

#endif // TABLE_H
