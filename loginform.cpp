#include "loginform.h"
#include <QDialogButtonBox>
#include <QGridLayout>
#include <QFormLayout>
#include <QLabel>

LoginForm::LoginForm(QWidget * parent) :
  QDialog(parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
  setWindowTitle(tr("Logon"));

  QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok|
                                    QDialogButtonBox::Cancel,
                                    Qt::Horizontal);

  QFormLayout *form = new QFormLayout();
  editUsername = new QLineEdit();
  editUsername->setPlaceholderText("username");
  editPassword = new QLineEdit();
  editPassword->setPlaceholderText("password");
  editPassword->setEchoMode(QLineEdit::Password);
  form->addRow(tr("&Username:"), editUsername);
  form->addRow(tr("&Password:"), editPassword);
  QGridLayout *grid = new QGridLayout(this);
  QLabel *icon = new QLabel("icon");
  icon->setPixmap(QPixmap(":/icons/login48x48.png"));
  grid->addWidget(icon, 0 ,0);
  grid->addLayout(form, 0, 1);
  grid->addWidget(buttons, 2, 1, 1, -1);

  connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
}
