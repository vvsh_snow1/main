#include <QtGui/QApplication>
#include <qtextcodec.h>
#include "mainwindow.h"
#include "connection.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

  if (!createConnection()) return 1;

  MainWindow w;
  w.resize(800,600);
  w.move(0,0);
  w.show();
  
  return a.exec();
}
