#ifndef EDITOR_H
#define EDITOR_H

#include <QSplitter>
#include "table.h"
#include "tableiomapper.h"

class QTableView;

class Editor : public QSplitter
{
  Q_OBJECT
public:
  explicit Editor(QWidget *parent = 0);
  ~Editor();
  QAction *windowMenuAction() const { return action; }
  void newFile();
  void focusInEvent ( QFocusEvent * event );
  void showEvent ( QShowEvent * event );
  bool event ( QEvent * e ) ;
  
signals:
  
private slots:
  void actionActivate();
  void showFormFilter();

private:
  QAction *action;
  QAction *filterAction;
  QString curFile;
  Table *table;
  TableIOMapper *mapper;
  QTableView *view;
};

#endif // EDITOR_H
