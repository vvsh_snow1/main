#include "filtersform.h"
#include <QDialogButtonBox>
#include <QGridLayout>

// ----------------------------------------------------------------------
FiltersForm::FiltersForm(TableSettings * ts, QWidget *parent) :
  QDialog(parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
  tableSettings = ts;
  setWindowTitle(QString::fromLocal8Bit("%1 :: �������").arg(ts->caption));
  createWidgets();
  createLayout();
  createConnections();
}
// ----------------------------------------------------------------------
void FiltersForm::accept()
{
  if (tableSettings) {
    for(int i = 0; i < tableSettings->filtersSettings.count(); ++i) {
      FilterSettings & fs = tableSettings->filtersSettings[i];
      const FilterIOMapper & m = mappers.at(i);
      if (!m.checkBox) continue;
      fs.state.isEnabled = m.checkBox->isChecked();
      if (m.comboBox1) fs.state.min = m.comboBox1->currentText();
      if (m.comboBox2) fs.state.max = m.comboBox2->currentText();
      if (m.dateTimeEdit1) fs.state.min = m.dateTimeEdit1->date();
      if (m.dateTimeEdit2) fs.state.max = m.dateTimeEdit2->date();
      fs.state.selectedGroupItem = -1;
      for (int i = 0; i < m.radioButtons.count(); ++i) {
        QRadioButton * rb = m.radioButtons.at(i);
        if (rb && rb->isChecked()) {
          fs.state.selectedGroupItem = i;
        }
      }
    }
  }
  QDialog::accept();
}
// ----------------------------------------------------------------------
void FiltersForm::createWidgets()
{
  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok|
                                   QDialogButtonBox::Cancel,
                                   Qt::Horizontal);
  if (!tableSettings) return;

  for(int i = 0; i < tableSettings->filtersSettings.count(); ++i) {
    const FilterSettings & fs = tableSettings->filtersSettings.at(i);
    FilterIOMapper m;
    m.checkBox = new QCheckBox;
    m.checkBox->setChecked(fs.state.isEnabled);
    m.checkBox->setDisabled(fs.isAlwaysEnabled);

    if (fs.type == "C") {
      m.comboBox1 = new QComboBox;
      m.comboBox1->setEditable(true);
      m.comboBox1->setEditText(fs.state.min.toString());
    }
    else if (fs.type == "L") {
      m.comboBox1 = new QComboBox;
      m.comboBox1->setEditable(true);
      m.comboBox1->setEditText(fs.state.min.toString());
    }
    else if (fs.type == "N") {
      m.comboBox1 = new QComboBox;
      m.comboBox1->setEditable(true);
      m.comboBox1->setEditText(fs.state.min.toString());

      m.comboBox2 = new QComboBox;
      m.comboBox2->setEditable(true);
      m.comboBox2->setEditText(fs.state.max.toString());
    }
    else if (fs.type == "D") {
      m.dateTimeEdit1 = new QDateTimeEdit(fs.state.min.toDate());
      m.dateTimeEdit1->setCalendarPopup(true);

      m.dateTimeEdit2 = new QDateTimeEdit(fs.state.max.toDate());
      m.dateTimeEdit2->setCalendarPopup(true);
    }
    else if (fs.type == "B" && fs.groupItems.count() > 0) {
      m.buttonGroup = new QButtonGroup();
      for (int j = 0; j < fs.groupItems.count(); ++j) {
        const FilterGroupItem & fgi = fs.groupItems.at(j);
        QRadioButton * rb = new QRadioButton();
        rb->setText(fgi.caption);
        rb->setChecked(fs.state.selectedGroupItem == j);
        m.buttonGroup->addButton(rb);
        m.radioButtons.append(rb);
      }
    }

    mappers.append(m);
  }
}
// ----------------------------------------------------------------------
void FiltersForm::createLayout()
{
  layout = new QGridLayout(this);

  for(int i = 0; i < mappers.count(); ++i) {
    FilterIOMapper m = mappers.at(i);
    FilterSettings fs = tableSettings->filtersSettings.at(i);

    QLabel * x;
    layout->addWidget(x = new QLabel(fs.caption, this), i, 0);

    if (m.checkBox) {
      layout->addWidget(m.checkBox, i, 1);
    }

    if (m.comboBox1 && m.comboBox2) {
      layout->addWidget(m.comboBox1, i, 2);
      layout->addWidget(m.comboBox2, i, 3, 1, -1);
    }
    else if (m.dateTimeEdit1 && m.dateTimeEdit2) {
      layout->addWidget(m.dateTimeEdit1, i, 2);
      layout->addWidget(m.dateTimeEdit2, i, 3, 1, -1);
    }
    else if (m.comboBox1 && !m.comboBox2) {
      layout->addWidget(m.comboBox1, i, 2, 1, -1);
    }
    else if (m.buttonGroup) {
      for (int j = 0; j < m.radioButtons.count(); ++j) {
        layout->addWidget(m.radioButtons.at(j), i, j + 2);
      }
    }
  }

  layout->setColumnStretch(2, 1);
  layout->addWidget(buttonBox, layout->rowCount(), 0, 1, -1);

  //setFixedHeight(height()); // �� ��������, �.�. ���� ��� �� ����������
}
// ----------------------------------------------------------------------
void FiltersForm::createConnections()
{
  connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}
// ----------------------------------------------------------------------
