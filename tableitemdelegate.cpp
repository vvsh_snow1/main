#include "tableitemdelegate.h"
#include "qdebug.h"

TableItemDelegate::TableItemDelegate(QObject *parent):
  QStyledItemDelegate(parent)
{
}

QWidget *TableItemDelegate::createEditor(QWidget *aParent,
                      const QStyleOptionViewItem &option,
                      const QModelIndex &index) const
{
  return QStyledItemDelegate::createEditor(aParent,option,index);
}

QString TableItemDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
  return QStyledItemDelegate::displayText(value, locale);
}

QSize TableItemDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
  QSize s = QStyledItemDelegate::sizeHint(option, index);
  if (index.row() == 0 && index.column() == 0 ) {
      qDebug() << s;
  }
  return s;
}
