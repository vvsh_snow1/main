#-------------------------------------------------
#
# Project created by QtCreator 2017-07-29T14:45:16
#
#-------------------------------------------------

# !On start app:
# -style=windows
# -style=motif
# -style=cde
# -style=plastique
# "windows", "motif", "cde", "plastique", "windowsxp", or "macintosh"
# Typically they include "windows", "motif", "cde", "plastique" and "cleanlooks". Depending on the platform, "windowsxp", "windowsvista" and "macintosh" may be available. Note that keys are case insensitive.

#CONFIG	    += console release debug
QT       += core gui sql

TARGET = testQT8
TEMPLATE = app

#DEFINES += QT_NO_DEBUG_OUTPUT

SOURCES += main.cpp \
    loginform.cpp \
    mainwindow.cpp \
    editor.cpp \
    treemodel.cpp \
    treeitem.cpp \
    customsqlmodel.cpp \
    table.cpp \
    tableiomapper.cpp \
    tableitemdelegate.cpp \
    vtableview.cpp \
    vsqlquerymodel.cpp \
    filtersform.cpp

HEADERS  += connection.h \
    loginform.h \
    settings.h \
    mainwindow.h \
    editor.h \
    treemodel.h \
    treeitem.h \
    customsqlmodel.h \
    table.h \
    tableiomapper.h \
    tableitemdelegate.h \
    vtableview.h \
    vsqlquerymodel.h \
    filtersform.h

RESOURCES     = main.qrc
RC_FILE       = main.rc
