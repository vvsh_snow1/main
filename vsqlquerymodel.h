#ifndef VSQLQUERYMODEL_H
#define VSQLQUERYMODEL_H

#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QString>
#include <QSqlIndex>

class VSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT

    QSqlIndex primaryIndex;
    QString tableName;
    //QSqlRecord rec;
public:
    VSqlQueryModel(QObject *parent = 0);

    void setTableName(const QString &tableName);
    QString getTableName() const;

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());

private:
    void refresh();

    enum Op { None, Insert, Update, Delete };
    //enum RemappingRowsState {  };

    struct ModifiedRow
    {
        ModifiedRow(Op o = None, const QSqlRecord &r = QSqlRecord()): op(o), rec(r) {}
        ModifiedRow(const ModifiedRow &other): op(other.op), rec(other.rec), primaryValues(other.primaryValues) {}
        Op op;
        QSqlRecord rec;
        QSqlRecord primaryValues;
    };

    typedef QMap<int, ModifiedRow> CacheMap;
    CacheMap cache;

    typedef QMap<int, int> RemappingRowsMap;
    mutable RemappingRowsMap remappingRows;
};

#endif // VSQLQUERYMODEL_H
