#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSqlQuery>
#include <QVector>
#include <QVariant>
#include <QString>
#include <QSqlError>
#include <QDebug>
#include <QDate>


struct FilterState
{
  FilterState(): isEnabled(false), selectedGroupItem(-1) {}
  bool isEnabled;
  QVariant min, max;
  int selectedGroupItem;
};
// ----------------------------------------------------------------------
struct FilterGroupItem
{
  FilterGroupItem() : isDisplayState(true) {}
  QString caption;
  QString sqlValue;
  bool isDisplayState;
};
// ----------------------------------------------------------------------
struct FilterSettings
{
  FilterSettings(): isAlwaysEnabled(false), isDisplayState(true) {}
  QString name;
  QString caption;
  QString fieldName;
  QString type;
  QString sqlValue;
  bool isAlwaysEnabled;
  bool isDisplayState;
  QString shortcut;
  QString lookupSql;
  QString lookupKeyField;

  typedef QVector<FilterGroupItem> FilterGroupItems;
  FilterGroupItems groupItems; // TODO: ������ �� ���������� �� ���������, �� �������� ����� ���������� ������� � ������

  FilterState state;
};
// ----------------------------------------------------------------------
struct FieldSettings
{
  FieldSettings(): length(0), index(-1), isVisible(true) {}
  QString name;
  QString caption;
  QString type;
  int     length;
  int     index;
  bool    isVisible;
};
// ----------------------------------------------------------------------
struct TableSettings
{
  QString tableId;
  QString caption;
  QString keyFields;
  QString sql;
  QString fieldsList;
  QString fieldsHidden;

  typedef QVector<FieldSettings> FieldsSettingsList;
  FieldsSettingsList fieldsSettings;

  typedef QVector<FilterSettings> FiltersSettingsList;
  FiltersSettingsList filtersSettings;

  QVector<int> FieldsIDs;
};
// ----------------------------------------------------------------------
static bool InitFiltersSettings(TableSettings & ts)
{
  Q_UNUSED(InitFiltersSettings);

  if (ts.tableId.isEmpty()) return false;

  QSqlQuery qry("", QSqlDatabase::database("settings"));

  qry.prepare("SELECT * FROM filters WHERE table_id=:ID ORDER BY filter_order, rowid");

  qry.bindValue(0, ts.tableId);

  if( !qry.exec() ) {
    qDebug() << qry.lastError();
    return false;
  }

  if( !qry.first() ) {
    qDebug() << "Failed to first";
    return false;
  }

  QMap<int, int> itemsMap;

  do {
    FilterSettings fs;

    fs.name = qry.value(0).toString();

    fs.caption = qry.value(2).toString();
    fs.fieldName = qry.value(3).toString();
    fs.type = qry.value(4).toString();
    fs.sqlValue = qry.value(5).toString();
    int groupID = qry.value(6).toInt();
    fs.isAlwaysEnabled = qry.value(8).toBool();
    bool isEnabled = qry.value(9).toBool();
    fs.state.isEnabled =  isEnabled || fs.isAlwaysEnabled;
    fs.isDisplayState = qry.value(10).toBool();
    fs.shortcut = qry.value(11).toString();
    fs.lookupSql = qry.value(12).toString();
    fs.lookupKeyField = qry.value(13).toString();

    if (groupID != 0) {
      FilterGroupItem gi;

      gi.caption = qry.value(7).toString();
      gi.sqlValue = fs.sqlValue;
      fs.sqlValue.clear();
      gi.isDisplayState = fs.isDisplayState;
      fs.isDisplayState = true;


      if (itemsMap.contains(groupID)) {
        int i = itemsMap.value(groupID);
        FilterSettings & fsi = ts.filtersSettings[i];
        fsi.groupItems.append(gi);
        if (isEnabled)
          fsi.state.selectedGroupItem = fsi.groupItems.count() - 1;
        continue;
      }
      else {
        itemsMap[groupID] = ts.filtersSettings.count();
        fs.groupItems.append(gi);
        if (isEnabled)
          fs.state.selectedGroupItem = fs.groupItems.count() - 1;
      }
    }

    ts.filtersSettings.append(fs);

  } while (qry.next());
/*
  for (int i = 0; i < ts.filtersSettings.count(); ++i) {
    FilterSettings fs = ts.filtersSettings.operator [](i);
    qDebug() << fs.name;
      for (int j = 0; j < fs.groupItems.count(); ++j)
        qDebug() << "    " << fs.groupItems[j].caption << " " << fs.groupItems[j].sqlValue;
   }
*/
  return true;
}
// ----------------------------------------------------------------------
static bool InitTableSettingsOnly(TableSettings & ts)
{
  Q_UNUSED(InitTableSettingsOnly);

  if (ts.tableId.isEmpty())
    return false;

  QSqlQuery qry("", QSqlDatabase::database("settings"));

  qry.prepare("SELECT * FROM tables WHERE table_id=:ID LIMIT 1");

  qry.bindValue(0, ts.tableId);

  if( !qry.exec() ) {
    qDebug() << qry.lastError();
    return false;
  }

  if( !qry.first() ) {
    qDebug() << "Failed to first";
    return false;
  }

  ts.tableId = qry.value(0).toString();
  ts.caption = qry.value(1).toString();
  ts.sql = qry.value(2).toString();
  ts.keyFields = qry.value(3).toString();
  ts.fieldsList = qry.value(4).toString();
  ts.fieldsHidden = qry.value(5).toString();

  return true;
}
// ----------------------------------------------------------------------
static bool InitFieldSettings(QString & aTableID, FieldSettings & fs)
{
  Q_UNUSED(InitFieldSettings);

  if (fs.name.isEmpty()) return false;

  QSqlQuery qry("", QSqlDatabase::database("settings"));

  qry.prepare("select * from fields where field_id = :FID1 and table_id = :TID "
              "union "
              "select * from fields where field_id = :FID2 and table_id = '' "
              "order by table_id desc");

  qry.bindValue(0, fs.name);
  qry.bindValue(1, aTableID);
  qry.bindValue(2, fs.name);

  if( !qry.exec() ) {
    qDebug() << qry.lastError();
    return false;
  }

  if( !qry.first() ) {
    return false;
  }

  fs.name = qry.value(0).toString();
  fs.caption = qry.value(2).toString();
  fs.type = qry.value(3).toString();
  fs.length = qry.value(4).toInt();
  fs.isVisible = qry.value(5).toBool();

  return true;
}
// ----------------------------------------------------------------------
static void GetFilterSql(const TableSettings & ts, QString & res, QString & resCaption)
{
  Q_UNUSED(GetFilterSql);

  for(int i = 0; i < ts.filtersSettings.count(); ++i) {
    const FilterSettings & fs = ts.filtersSettings.at(i);
    if (!fs.state.isEnabled) continue;
      QString curSql, curText;

      if (!fs.sqlValue.isEmpty()) {
        curSql = fs.sqlValue;
        if (fs.isDisplayState) curText = fs.caption;
      }
      else if (fs.state.selectedGroupItem > -1) {
        const FilterGroupItem & gi = fs.groupItems.at(fs.state.selectedGroupItem);
        if (gi.sqlValue.isEmpty()) continue;
        curSql = gi.sqlValue;
        if (gi.isDisplayState) curText = QString("%1: %2").arg(fs.caption, gi.caption);
      }
      else {
        QString curSqlFld, curTextFld;
        if (fs.fieldName.isEmpty()) curSqlFld = fs.name; else curSqlFld = fs.fieldName;
        if (fs.caption.isEmpty()) curTextFld = fs.name; else curTextFld = fs.caption;
        if (curSqlFld.isEmpty()) continue;

        if (fs.state.min.type() == QVariant::Date) {
          QString curMin(fs.state.min.toDate().toString("yyyyMMdd")); // TODO: ������� � ���������
          if (fs.state.min.toDate() < fs.state.max.toDate()) {
            QString curMax(fs.state.max.toDate().toString("yyyyMMdd"));
            curSql = QString("%1 BETWEEN %2 AND %3").arg(curSqlFld, curMin, curMax);
            if (fs.isDisplayState)
              curText = QString::fromLocal8Bit("%1: %2�%3").arg(curTextFld,
                                                              fs.state.min.toDate().toString(Qt::SystemLocaleShortDate),
                                                              fs.state.max.toDate().toString(Qt::SystemLocaleShortDate));
          }
          else {
            curSql = QString("%1=%2").arg(curSqlFld, curMin);
            if (fs.isDisplayState)
              curText = QString("%1: %2").arg(curTextFld,
                                            fs.state.min.toDate().toString(Qt::SystemLocaleShortDate));
          }
        }
        else {
          bool isMinL, isMinD;
          fs.state.min.toLongLong(&isMinL);
          fs.state.min.toDouble(&isMinD);

          if (isMinL || isMinD) {
            bool isMaxL, isMaxD;
            fs.state.max.toLongLong(&isMaxL);
            fs.state.max.toDouble(&isMaxD);
            QString curMin(fs.state.min.toString().simplified());
            //if (isMinD) //TODO: ������ �����
            if (isMaxL || isMaxD) {
              QString curMax(fs.state.max.toString().simplified());
              //if (isMaxD) //TODO: ������ �����
              curSql = QString("%1 BETWEEN %2 AND %3").arg(curSqlFld, curMin, curMax);
              if (fs.isDisplayState)
                curText = QString::fromLocal8Bit("%1: %2�%3").arg(curTextFld,
                                                                fs.state.min.toString().simplified(),
                                                                fs.state.max.toString().simplified());
            }
            else {
              curSql = QString("%1=%2").arg(curSqlFld, curMin);
              if (fs.isDisplayState)
                curText = QString("%1: %2").arg(curTextFld, fs.state.min.toString().simplified());
            }
          }
          else {
            QString curMin(fs.state.min.toString());
            //TODO: ������������ ESC
            curSql = QString("%1 LIKE '%2'").arg(curSqlFld, curMin);
            if (fs.isDisplayState)
              curText = QString("%1: '%2'").arg(curTextFld, fs.state.min.toString());
          }
        }
      }

    if (!res.isEmpty() && !curSql.isEmpty()) res.append(" AND ");
    res.append(curSql);

    if (!resCaption.isEmpty() && !curText.isEmpty()) resCaption.append(" + ");
    resCaption.append(curText);
  }
}
// ----------------------------------------------------------------------
#endif // SETTINGS_H
