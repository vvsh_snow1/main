#include "table.h"
#include "settings.h"
#include <QtSql>

Table::Table()
{
  //m_model = new QSqlQueryModel(0);
  m_model = new CustomSqlModel(0);
  //m_model = new VSqlQueryModel();
  m_model->m_table = this;
}
// ----------------------------------------------------------------------
Table::~Table()
{
  delete m_model;
}
// ----------------------------------------------------------------------
bool Table::openTable()
{
  tableSettings.filtersSettings.clear();
  InitFiltersSettings(tableSettings);

  QString querySql(tableSettings.sql);

  if (querySql.contains("%FILTER")) {
    QString filterSql, filterText;
    GetFilterSql(tableSettings, filterSql, filterText);
    if (filterSql.isEmpty()) filterSql = "1=1";
    querySql.replace("%FILTER", filterSql);
  }

  m_model->setQuery(querySql);

  if ( !m_model->query().isActive() ) {
    qDebug() << m_model->lastError();
    return false;
  }

  QSqlRecord rec = m_model->record();

  qDebug() << rec;

  tableSettings.fieldsSettings.clear();

  for (int i = 0; i < rec.count(); ++i) {
    QString fn = rec.fieldName(i);
    FieldSettings fs;
    fs.name = fn;
    if (InitFieldSettings(tableSettings.tableId, fs)) {
      tableSettings.fieldsSettings.append(fs);
    }
    else {
      FieldSettings fsNull;
      tableSettings.fieldsSettings.append(fsNull);
    }
  }

  if ( !tableSettings.fieldsList.isEmpty() ) {
    QStringList fl = tableSettings.fieldsList.simplified().split(",", QString::SkipEmptyParts);

    if ( !fl.isEmpty() ) {
      fl.replaceInStrings(" ", "");
      for (int i = 0; i < rec.count(); ++i) {
        QString fn = rec.fieldName(i);
        int fi = fl.indexOf(fn);
        FieldSettings & fs = tableSettings.fieldsSettings[i];
        fs.name = fn;
        fs.isVisible = fi > -1;
        fs.index = fi;
      }
    }
  }


  //m_model->select();
  /*
  m_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
  if (!m_model->select())
          return false;
  names->clear();
  for (int i =0; i < m_model->columnCount(); i++)
      names->append(m_model->headerData(i, Qt::Horizontal).toString());
  return true;
  */
  return true;
}

bool Table::reopenTable()
{
  QString querySql(tableSettings.sql);

  if (querySql.contains("%FILTER")) {
    QString filterSql, filterText;
    GetFilterSql(tableSettings, filterSql, filterText);
    if (filterSql.isEmpty()) filterSql = "1=1";
    querySql.replace("%FILTER", filterSql);
  }

  qDebug() << querySql;
  m_model->setQuery(querySql);

  if ( !m_model->query().isActive() ) {
    qDebug() << m_model->lastError();
    return false;
  }

  return true;
}
// ----------------------------------------------------------------------
