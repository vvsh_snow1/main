#include "tableiomapper.h"
#include <QDataWidgetMapper>
#include <QAbstractItemView>

TableIOMapper::TableIOMapper(Table *table) :
  QObject(0)
{
  m_table = table;
  mapper = new QDataWidgetMapper();
  m_proxy = new QSortFilterProxyModel();
  m_proxy->setSortCaseSensitivity(Qt::CaseInsensitive);
  m_proxy->setSourceModel(m_table->m_model);
  mapper->setModel(m_proxy);
}
// ----------------------------------------------------------------------
TableIOMapper::~TableIOMapper()
{
  delete mapper;
  delete m_proxy;
}
// ----------------------------------------------------------------------
void TableIOMapper::setView(QAbstractItemView * view)
{
  view->setModel(m_proxy);
  //view->setItemDelegate(new QSqlRelationalDelegate());
}
// ----------------------------------------------------------------------
void TableIOMapper::refresh()
{

}
// ----------------------------------------------------------------------
