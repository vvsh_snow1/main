#ifndef TABLEIOMAPPER_H
#define TABLEIOMAPPER_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "table.h"

class QDataWidgetMapper;
class QAbstractItemView;

class TableIOMapper : public QObject
{
  Q_OBJECT
public:
  explicit TableIOMapper(Table *table);
  ~TableIOMapper();
  void setView(QAbstractItemView * view);
signals:
  
public slots:
  void refresh();

protected:
  QDataWidgetMapper * mapper;
  Table * m_table;
  QSortFilterProxyModel * m_proxy;
};

#endif // TABLEIOMAPPER_H
