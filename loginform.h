#ifndef LOGINFORM_H
#define LOGINFORM_H

#include <QDialog>
#include <QLineEdit>

class LoginForm : public QDialog
{
  Q_OBJECT
public:
  LoginForm(QWidget * parent = 0);
  // ----------------------------------------------------------------------
  QString UserName() const
  {
    if(editUsername) return editUsername->text();
    else return QString();
  }
  // ----------------------------------------------------------------------
  void setUserName(const QString & aUsername)
  {
    if(editUsername) editUsername->setText(aUsername);
  }
  // ----------------------------------------------------------------------
  QString Password()
  {
    if(editPassword) return editPassword->text();
    else return QString();
  }
  // ----------------------------------------------------------------------
  void setPassword(const QString & aPassword)
  {
    if(editPassword) editPassword->setText(aPassword);
  }
  // ----------------------------------------------------------------------

private:
  QLineEdit * editUsername;
  QLineEdit * editPassword;
};

#endif // LOGINFORM_H
