//#include "qdebug.h"

#include "customsqlmodel.h"
#include "settings.h"
#include "table.h"
#include <QSqlRecord>
#include <QSqlField>

CustomSqlModel::CustomSqlModel(QObject *parent)
    : VSqlQueryModel(parent)
{
  m_table = 0;
}

QVariant CustomSqlModel::data(const QModelIndex &index, int role) const
{
  QVariant value = VSqlQueryModel::data(index, role);

  if (!m_table) return value;

  FieldSettings fs = m_table->tableSettings.fieldsSettings.at(index.column());

  if (fs.name == record().fieldName(index.column())) {
    if (fs.type == "B") {
      if (role == Qt::CheckStateRole) {
        value = VSqlQueryModel::data(index, Qt::DisplayRole);

        if (value == "Y") return Qt::Checked;
        else if (value == "N") return Qt::Unchecked;
        else return Qt::PartiallyChecked;
      }
      else if (role == Qt::DisplayRole) {
        if (value == "Y") return "Yes";
        else if (value == "N") return "No";
      }
    }
  }

  if (role == Qt::DisplayRole) {
    QVariant::Type t = record().field(index.column()).type();
    if (t == QVariant::Double)
      return value.toString();
  }
  else if (role == Qt::TextAlignmentRole) {
    QVariant::Type t = record().field(index.column()).type();
    if (t == QVariant::Double || t == QVariant::Int
        || t == QVariant::UInt || t == QVariant::ULongLong)
      return int (Qt::AlignRight | Qt::AlignVCenter);
  }

  // !!! Qt::SizeHintRole �� ���������
//  if (/*role == Qt::SizeHintRole &&*/ index.column() == 0 && index.row() == 0) {
//    qDebug() << role << value;
//  }

  return value;
/*
    QVariant value = QSqlQueryModel::data(index, role);
    if (value.isValid() && role == Qt::DisplayRole) {
        if (index.column() == 0)
            return value.toString().prepend("#");
        else if (index.column() == 2)
            return value.toString().toUpper();
    }
    if (role == Qt::TextColorRole && index.column() == 1)
        return QVariant::fromValue(QColor(Qt::blue));
    return value;
*/
}
