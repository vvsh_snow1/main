#include <QtGui>

#include "editor.h"
#include "mainwindow.h"
#include "treemodel.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
{
  mdiArea = new QMdiArea(this);
  mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  setCentralWidget(mdiArea);
  connect(mdiArea, SIGNAL(subWindowActivated(QMdiSubWindow*)),
          this, SLOT(updateActions()));

  createActions();
  createDockWindows();
  createMenus();
  createToolBars();
}
// ----------------------------------------------------------------------
MainWindow::~MainWindow()
{
  
}
// ----------------------------------------------------------------------
void MainWindow::createDockWindows()
{
  model1 = new TreeModel(this);

  QDockWidget *dock = new QDockWidget(tr("Tree"), this);
  dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  QTreeView *customerList = new QTreeView(dock);
  //customerList->setAlternatingRowColors(true);
  customerList->setModel(model1);
  dock->setWidget(customerList);
  addDockWidget(Qt::LeftDockWidgetArea, dock);
  // viewMenu->addAction(dock->toggleViewAction());
  connect(customerList, SIGNAL(activated(const QModelIndex&)),
                    SLOT(showView(const QModelIndex&)));
  //connect(customerList, SIGNAL(doubleClicked(const QModelIndex&)),
  //                  SLOT(showView(const QModelIndex&)));
}
// ----------------------------------------------------------------------
void MainWindow::createToolBars()
{
  fileToolBar = addToolBar(tr("View"));
  fileToolBar->addAction(newAction);
  fileToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}
// ----------------------------------------------------------------------
void MainWindow::createMenus()
{
  windowMenu = menuBar()->addMenu(tr("View"));
  windowMenu->addAction(newAction);
  windowMenu->addSeparator();
}
// ----------------------------------------------------------------------
void MainWindow::createActions()
{
  newAction = new QAction(tr("&New"), this);
  //newAction->setIcon(QIcon(":/images/new.png"));
  newAction->setShortcut(QKeySequence::New);
  newAction->setStatusTip(tr("Create a new file"));
  connect(newAction, SIGNAL(triggered()), this, SLOT(newFile()));
  windowActionGroup = new QActionGroup(this);
}
// ----------------------------------------------------------------------
void MainWindow::newFile()
{
  Editor *editor = new Editor(this);
  editor->newFile();
  addEditor(editor);
}
// ----------------------------------------------------------------------
void MainWindow::addEditor(Editor *editor)
{
  //connect(editor, SIGNAL(copyAvailable(bool)),
  //        cutAction, SLOT(setEnabled(bool)));
  //connect(editor, SIGNAL(copyAvailable(bool)),
  //        copyAction, SLOT(setEnabled(bool)));


  QMdiSubWindow *subWindow = mdiArea->addSubWindow(editor);
  windowMenu->addAction(editor->windowMenuAction());
  fileToolBar->addAction(editor->windowMenuAction());
  windowActionGroup->addAction(editor->windowMenuAction());
  subWindow->showMaximized();
}
// ----------------------------------------------------------------------
void MainWindow::updateActions()
{
  if (activeEditor())
    activeEditor()->windowMenuAction()->setChecked(true);
}
// ----------------------------------------------------------------------
Editor *MainWindow::activeEditor()
{
    QMdiSubWindow *subWindow = mdiArea->activeSubWindow();
    if (subWindow)
      return qobject_cast<Editor *>(subWindow->widget());
    return 0;
}
// ----------------------------------------------------------------------
void MainWindow::showView(const QModelIndex& index)
{
  QVariant tmp = model1->data(index, Qt::UserRole);
  QString command = tmp.toString();
  if (command.isEmpty())
    return;
  //qDebug() << "cmd: " << command;

  /*
  QSqlQuery qry("", QSqlDatabase::database("settings"));

  qry.prepare("SELECT * FROM tables WHERE table_id=:command");
  qry.bindValue(0, command);
  if( !qry.exec() ) {
    //qFatal( "Failed to exec" );
    qDebug() << qry.lastError();
    return;
  }
  if( !qry.next() ) {
    //qFatal( "Failed to next" );
    qDebug() << "Failed to next";
    return;
  }
  */

  for (int i = 0; i < mdiArea->subWindowList().count(); ++i) {
    QMdiSubWindow *cur = mdiArea->subWindowList().at(i);
    QWidget *curw = cur->widget();
    if (curw->property("table") == command) {
      if (!isActiveWindow())
        activateWindow();

      if (cur->windowState() & Qt::WindowMinimized)
        cur->showNormal();

      curw->setFocus();
      return;
    }
  }

  Editor *editor = new Editor(this);
  editor->setProperty("table", command);
  editor->newFile();
  addEditor(editor);
}
// ----------------------------------------------------------------------
