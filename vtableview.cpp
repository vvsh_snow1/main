#include <QTableView>
#include <QKeyEvent>
#include "vtableview.h"
//---------------------------------------------------------------------------
void VTableView::keyPressEvent(QKeyEvent *event)
{
  switch (event->key()) {
  case Qt::Key_Delete: // Delete record
    if (event->modifiers() & Qt::ControlModifier) {
      model()->removeRows(currentIndex().row(), 1);
      qDebug("Key_Delete  ControlModifier");
    }
    break;
  case Qt::Key_Down: // Insert record
    if (event->modifiers() == Qt::NoModifier) {
      if (currentIndex().row() + 1 == model()->rowCount()) {
        model()->insertRow(model()->rowCount());
        selectionModel()->select(currentIndex(), QItemSelectionModel::ClearAndSelect);
        qDebug("Key_Down  NoModifier");
      }
    }
    break;
  }

  QTableView::keyPressEvent(event);
}
//---------------------------------------------------------------------------
