#ifndef CONNECTION_H
#define CONNECTION_H

#include <QSqlDatabase>
#include <QMessageBox>
#include <QSettings>
#include "loginform.h"

#define DB_SETTINGS "settings"

static bool createConnection()
{
  QSqlDatabase dbs = QSqlDatabase::addDatabase("QSQLITE", DB_SETTINGS);

  dbs.setDatabaseName( "./settings.db" );

  if( !dbs.open() )
  {
    qDebug() << dbs.lastError();
    //qFatal( "Failed to connect." ); //runtime error
    QMessageBox::critical(0, qApp->tr("Cannot open database"),
        dbs.lastError().text(), QMessageBox::Cancel);
    return false;
  }

  QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

  QSettings cfg("./settings.ini", QSettings::IniFormat);


  cfg.beginGroup("DB");

  LoginForm loginForm(0);



  db.setHostName(cfg.value("HostName", "").toString());
  db.setDatabaseName(cfg.value("DatabaseName", "test").toString());
  loginForm.setUserName(cfg.value("UserName", "root").toString());
  loginForm.setPassword(cfg.value("Password", "0").toString());
  db.setPort(cfg.value("Port", 3307).toInt());

  if (!loginForm.exec()) return false;

  db.setUserName(loginForm.UserName());
  db.setPassword(loginForm.Password());

  cfg.endGroup();

  if (!db.open()) {
      qDebug() << db.lastError();
      //qFatal( "Failed to connect." ); //runtime error
      QMessageBox::critical(0, qApp->tr("Cannot open database"),
          db.lastError().text(), QMessageBox::Cancel);
      return false;
  }

  return true;
}

#endif // CONNECTION_H
