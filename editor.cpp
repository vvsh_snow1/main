#include <QtGui>
#include <QEvent>
#include <QtSql>
#include "editor.h"
#include "filtersform.h"
#include "table.h"
#include "tableiomapper.h"
#include "settings.h"
#include "tableitemdelegate.h"
#include "vtableview.h"

Editor::Editor(QWidget *parent) :
  QSplitter(parent)
{
  table = new Table();
  mapper = new TableIOMapper(table);

  view = new VTableView(this);
  view->setSortingEnabled(true);
  view->horizontalHeader()->setSortIndicator(-1, Qt::AscendingOrder);
  view->setWordWrap(false);
  TableItemDelegate *delagate = new TableItemDelegate(view);
  view->setItemDelegate(delagate);
  view->horizontalHeader()->setMovable(true);
  view->verticalHeader()->setHidden(true);
  int h = view->fontMetrics().height();
  h = h + h / 2;
  view->verticalHeader()->setDefaultSectionSize(h);

  //view->horizontalHeader()->setStretchLastSection(true);
  //view->setAlternatingRowColors(true);
  //view->setShowGrid(false);

  view->setDisabled(true);

  mapper->setView(view);
 // addWidget(view); // ���������� ��� �������� �������


//  addWidget(new QTextEdit(this));
//  setOrientation(Qt::Vertical);
//  setStretchFactor(1, 1);

  action = new QAction(this);
  action->setCheckable(true);
  //connect(action, SIGNAL(triggered()), this, SLOT(show()));
  //connect(action, SIGNAL(triggered()), this, SLOT(setFocus()));
  connect(action, SIGNAL(triggered()), this, SLOT(actionActivate()));
  //setWindowTitle("[*]");
 // filterAction = new QAction(view);
 // filterAction->setShortcut(QKeySequence("F4"));
 // connect(filterAction, SIGNAL(triggered()), this, SLOT(showFormFilter()));

  QShortcut * shortcut = new QShortcut(QKeySequence("F4"), this);
  connect(shortcut, SIGNAL(activated()), this, SLOT(showFormFilter()));

  setAttribute(Qt::WA_DeleteOnClose);
}
// ----------------------------------------------------------------------
Editor::~Editor()
{
  delete mapper;
  delete table;
}
// ----------------------------------------------------------------------
void Editor::actionActivate()
{
  if (windowState() & Qt::WindowMinimized) showNormal();
  setFocus();
}
// ----------------------------------------------------------------------
void Editor::showFormFilter()
{
  // TODO: ���������� ������ ���� ������ �� ��������� ���� ��� ��������
  if (table->tableSettings.filtersSettings.count() < 1) return;
  FiltersForm filtersForm(&table->tableSettings, this);
  if (!filtersForm.exec()) return;

  table->reopenTable();
}
// ----------------------------------------------------------------------
void Editor::newFile()
{
  // TODO: �������� ���-�� �������������� �������������, ���-�� � ����������� ��������� � �.�.
  if (!table) return;

  table->tableSettings.tableId = property("table").toString();

  if (!InitTableSettingsOnly(table->tableSettings)) return;

  setWindowTitle(table->tableSettings.caption);
  action->setText(table->tableSettings.caption);

  QWidget* w = (QWidget*)this;
  QIcon icon = const_cast<QWidget*>(w)->style()->standardIcon(QStyle::SP_FileIcon);
  action->setIcon(icon);
  action->setIconText(table->tableSettings.caption);

  QTime t;
  t.start();

  if ( !table->openTable() ) return;
  //qDebug("open Query Time elapsed: %d ms", t.elapsed());

  for (int i = 0; i < view->model()->columnCount(); ++i) {
    FieldSettings fs = table->tableSettings.fieldsSettings[i];
    QString cn = view->model()->headerData(i, Qt::Horizontal).toString();
    if (fs.name == cn) {
      if (!fs.caption.isEmpty()) {
        view->model()->setHeaderData(i, Qt::Horizontal, fs.caption);
      }

      if (fs.length > 0) {
        int w = view->fontMetrics().width("W") * fs.length;
        view->setColumnWidth(i, w);
      }

      if (!fs.isVisible) {
        view->hideColumn(i);
      }
      else if (fs.index != i && fs.index > -1) {
        view->horizontalHeader()->moveSection(i, fs.index);
      }
    }
  }

  //view->resizeRowsToContents(); // !!! �������� ����� - 3��� �� 20��� ��� � 10 ���.

  view->setEnabled(true);
  addWidget(view);

  qDebug("show Query Time elapsed: %d ms", t.elapsed());
  /*
  static int documentNumber = 1;

  curFile = tr("document%1.txt").arg(documentNumber);
  setWindowTitle(curFile + "[*]");
  action->setText(curFile);
  //isUntitled = true;
  ++documentNumber;
  */
}
// ----------------------------------------------------------------------
void Editor::focusInEvent ( QFocusEvent * event )
{
 // action->setChecked(true);
   //qDebug("focusInEvent");
   QWidget::focusInEvent(event);
}
// ----------------------------------------------------------------------
void Editor::showEvent ( QShowEvent * event )
{
  //action->setChecked(true);
 // qDebug("showEvent");
  QWidget::showEvent(event);
}
// ----------------------------------------------------------------------
bool Editor::event ( QEvent * e )
{
//  qDebug("%d", e->type());
  return QWidget::event(e);
}
// ----------------------------------------------------------------------
