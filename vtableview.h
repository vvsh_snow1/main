#ifndef VTABLEVIEW_H
#define VTABLEVIEW_H

#include <QTableView>

class VTableView : public QTableView
{
public:
  VTableView(QWidget *parent = 0)
    : QTableView(parent) {}
protected:
  void keyPressEvent(QKeyEvent *event);
};

#endif // VTABLEVIEW_H
