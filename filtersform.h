#ifndef FILTERSFORM_H
#define FILTERSFORM_H

#include <QDialog>
#include <QCheckBox>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QRadioButton>
#include <QLabel>
#include <QButtonGroup>
#include "settings.h"

class QDialogButtonBox;
class QGridLayout;

class FiltersForm : public QDialog
{
  Q_OBJECT
public:
  FiltersForm(TableSettings * ts, QWidget *parent);

signals:
  
public slots:
    void accept();

private:
    void createWidgets();
    void createLayout();
    void createConnections();

    QDialogButtonBox *buttonBox;
    QGridLayout *layout;

    TableSettings * tableSettings;
  
    struct FilterIOMapper
    {
      FilterIOMapper() :
        checkBox(0),
        comboBoxOp(0),
        comboBox1(0),
        comboBox2(0),
        dateTimeEdit1(0),
        dateTimeEdit2(0),
        buttonGroup(0) {}
      QCheckBox * checkBox;
      QComboBox * comboBoxOp;
      QComboBox * comboBox1;
      QComboBox * comboBox2;
      QDateTimeEdit * dateTimeEdit1;
      QDateTimeEdit * dateTimeEdit2;
      QButtonGroup * buttonGroup;
      QVector<QRadioButton *> radioButtons;
    };
    typedef QVector<FilterIOMapper> FiltersIOMappers;
    FiltersIOMappers mappers;
};

#endif // FILTERSFORM_H
