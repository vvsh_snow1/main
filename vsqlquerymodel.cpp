#include <QtSql>
#include <QDebug>
#include "vsqlquerymodel.h"
//---------------------------------------------------------------------------
VSqlQueryModel::VSqlQueryModel(QObject *parent)
    : QSqlQueryModel(parent)
{
}
//---------------------------------------------------------------------------
void VSqlQueryModel::setTableName(const QString &tableName)
{
  this->tableName = tableName;
  primaryIndex = query().driver()->primaryIndex(this->tableName);
  //rec = query().driver()->record(this->tableName);
  //qDebug() << rec;
}
//---------------------------------------------------------------------------
QString VSqlQueryModel::getTableName() const
{
  return tableName;
}
//---------------------------------------------------------------------------
Qt::ItemFlags VSqlQueryModel::flags(
        const QModelIndex &index) const
{
  /*

    Q_D(const QSqlTableModel);
    if (index.internalPointer() || index.column() < 0 || index.column() >= d->rec.count()
        || index.row() < 0)
        return 0;
    if (d->rec.field(index.column()).isReadOnly())
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
 */
  if (index.internalPointer() || index.column() < 0 || index.column() >= query().record().count()
      || index.row() < 0)
      return 0;
  if (query().record().field(index.column()).isReadOnly())
      return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
  /*
    Qt::ItemFlags flags = QSqlQueryModel::flags(index);
//    if (index.column() == 1 || index.column() == 2)
        flags |= Qt::ItemIsEditable;
    return flags;
    */
}
//---------------------------------------------------------------------------
bool VSqlQueryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
//    if (index.column() < 1 || index.column() > 2)
//        return false;

//    QModelIndex primaryKeyIndex = QSqlQueryModel::index(index.row(), 0);
//    int id = data(primaryKeyIndex).toInt();

//    clear();

//    bool ok;
//    if (index.column() == 1) {
//        ok = setFirstName(id, value.toString());
//    } else {
//        ok = setLastName(id, value.toString());
//    }
//    refresh();
//    return ok;

  if (role != Qt::EditRole)
      return QSqlQueryModel::setData(index, value, role);

  if (!index.isValid() || index.column() >= query().record().count() || index.row() >= rowCount())
      return false;


  int realRow;
  if (remappingRows.contains(index.row())) {
    realRow = remappingRows.value(index.row());
  }
  else {
    return false;
  }

  ModifiedRow &row = cache[realRow];
  if (row.op == None) {
      row.op = Update;
      if(query().seek(realRow)) {
        row.rec = query().record();
        //row.primaryValues = d->primaryValues(indexInQuery(index).row());
//        row.rec.setValue(index.column(), value);
//        emit dataChanged(index, index);
//        return true;
      }
      else {
        return false;
      }
  }

  row.rec.setValue(index.column(), value);
  //row.rec.setGenerated(index.column(), true);

  emit dataChanged(index, index);
  return true;

/* // ���� ��� ��������
  if (role != Qt::EditRole)
      return QSqlQueryModel::setData(index, value, role);

  if (!index.isValid() || index.column() >= query().record().count() || index.row() >= rowCount())
      return false;

  ModifiedRow &row = cache[index.row()];
  if (row.op == None) {
      row.op = Update;
      if(query().seek(index.row())) {
        row.rec = query().record();
        //row.primaryValues = d->primaryValues(indexInQuery(index).row());
//        row.rec.setValue(index.column(), value);
//        emit dataChanged(index, index);
//        return true;
      }
      else {
        return false;
      }
  }

  row.rec.setValue(index.column(), value);
  //row.rec.setGenerated(index.column(), true);

  emit dataChanged(index, index);
  return true;
*/
}
//---------------------------------------------------------------------------
QVariant VSqlQueryModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid() || (role != Qt::DisplayRole && role != Qt::EditRole))
      return QVariant();

  // Problem.. we need to use QSQM::indexInQuery to handle inserted columns
  // but inserted rows we need to handle
  // and indexInQuery is not virtual (grrr) so any values we pass to QSQM need
  // to handle the insertedRows
 // QModelIndex item = indexInQuery(index);


  // !!! ������ �������� ����������, ���� ���-�� ������, ��� ����� �������, �������� � ���������������
  //if (remappingRows.isEmpty() && cache.isEmpty()) return QSqlQueryModel::data(index, role);

  if (remappingRows.contains(index.row())) {
    int realRow = remappingRows.value(index.row());

    const ModifiedRow row = cache.value(realRow);
    const QVariant var = row.rec.value(index.column());

    if (var.isValid() || row.op == Insert)
      return var;

    QModelIndex realIndex = createIndex(realRow, index.column());
    return QSqlQueryModel::data(realIndex, role);
  }
  else {
    query().first();

    int currRow, i = -1;

    do {
       currRow = query().at();

       const ModifiedRow row = cache.value(currRow);
       if (row.op == Delete) {
          continue;
       }
       ++i;

       if (i == index.row()) {
         remappingRows[index.row()] = currRow;
         //qDebug() << "index.row()=" <<index.row() << "currRow =" << currRow;
         const QVariant var = row.rec.value(index.column());
         if (var.isValid()) return var;
         return query().value(index.column());
       }
    } while (query().next());

    for (CacheMap::ConstIterator it = cache.constBegin(); it != cache.constEnd(); ++it) {
      if (it.value().op == VSqlQueryModel::Insert) {
        ++i;
        if (i == index.row()) {
          remappingRows[index.row()] = it.key();

          const QVariant var = it.value().rec.value(index.column());
          return var;
        }
      }
    }
  }

  return QVariant();

/* // ���� ��� ��������
  const ModifiedRow row = cache.value(index.row());
  const QVariant var = row.rec.value(index.column());

  if (var.isValid() || row.op == Insert)
    return var;

  return QSqlQueryModel::data(index, role); // ���� �� ���� �����������
*/
  // We need to handle row mapping here, but not column mapping
  //return QSqlQueryModel::data(index.sibling(item.row(), index.column()), role);
}
//---------------------------------------------------------------------------
int VSqlQueryModel::rowCount(const QModelIndex &) const
{
  int rc = QSqlQueryModel::rowCount();

  for (CacheMap::ConstIterator it = cache.constBegin(); it != cache.constEnd(); ++it) {
    if (it.value().op == VSqlQueryModel::Insert) {
      ++rc;
    }
    else if (it.value().op == VSqlQueryModel::Delete) {
      --rc;
    }
  }

  return rc;
}
//---------------------------------------------------------------------------
bool VSqlQueryModel::removeRows(int row, int count, const QModelIndex &parent)
{
  if (parent.isValid() || row < 0 || count <= 0)
      return false;

  int realRow;
  if (remappingRows.contains(row)) {
    realRow = remappingRows.value(row);
  }
  else {
    return false;
  }

  //emit beforeDelete(row);
  beginRemoveRows(parent, row, row);

  ModifiedRow &drow = cache[realRow];
  if (drow.op == None) {
      drow.op = Delete;
      if(query().seek(realRow)) {
       // row.rec = query().record();
        //row.primaryValues = d->primaryValues(indexInQuery(index).row());
      }
  }
  else if (drow.op == Update) {
    drow.op = Delete;
  }
  else if (drow.op == Insert) {
    cache.remove(realRow);
  }
  else {
    return false;
  }

  remappingRows.clear();

  endRemoveRows();
  emit headerDataChanged(Qt::Vertical, row, row);
  return true;


  //return QSqlQueryModel::removeRows(row, count, parent);
}
//---------------------------------------------------------------------------
bool VSqlQueryModel::insertRows(int row, int count, const QModelIndex &parent)
{

  int maxInsertedRow = -1;
  for (CacheMap::ConstIterator it = cache.constBegin(); it != cache.constEnd(); ++it) {
    if (it.value().op == VSqlQueryModel::Insert) {
      if (it.key() > maxInsertedRow) maxInsertedRow = it.key();
    }
  }

  int realRow = rowCount(), cacheRow = QSqlQueryModel::rowCount();
  if (maxInsertedRow > -1) cacheRow = maxInsertedRow + 1;
  beginInsertRows(parent, realRow, realRow);
  ModifiedRow &mrow = cache[cacheRow];
  mrow.op = Insert;
  mrow.rec = this->record();
  remappingRows[realRow] = cacheRow;
//  emit primeInsert(c, cache[c].rec);
  endInsertRows();
  return true;
  //return QSqlQueryModel::insertRows(row, count, parent);



/*   // ���� ��� ��������
  int rc = rowCount();
  beginInsertRows(parent, rc, rc);
  ModifiedRow &mrow = cache[rc];
  mrow.op = Insert;
  mrow.rec = rec;
//  emit primeInsert(c, cache[c].rec);
  endInsertRows();
  return true;
  //return QSqlQueryModel::insertRows(row, count, parent);
*/
}
//---------------------------------------------------------------------------
void VSqlQueryModel::refresh()
{
//    setQuery("select * from person");
//    setHeaderData(0, Qt::Horizontal, QObject::tr("ID"));
//    setHeaderData(1, Qt::Horizontal, QObject::tr("First name"));
//    setHeaderData(2, Qt::Horizontal, QObject::tr("Last name"));
}
//---------------------------------------------------------------------------
