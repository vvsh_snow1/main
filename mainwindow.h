#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "treemodel.h"

class Editor;
class QActionGroup;
class QMdiArea;

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
    void newFile();
    void updateActions();
protected slots:
    void showView(const QModelIndex& index);

private:
  QMdiArea *mdiArea;
  QMenu *windowMenu;
  QActionGroup *windowActionGroup;
  QAction *newAction;
  QToolBar *fileToolBar;
  TreeModel *model1;
  void createActions();
  void createDockWindows();
  void createToolBars();
  void createMenus();
  void addEditor(Editor *editor);
  Editor *activeEditor();
};

#endif // MAINWINDOW_H
