#ifndef CUSTOMSQLMODEL_H
#define CUSTOMSQLMODEL_H

//#include <QSqlQueryModel>
#include "vsqlquerymodel.h"

class Table;
class CustomSqlModel : public VSqlQueryModel
{
    Q_OBJECT

public:
    CustomSqlModel(QObject *parent = 0);

    QVariant data(const QModelIndex &item, int role) const;
    Table *m_table;
};

#endif
