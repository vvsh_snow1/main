#ifndef TABLEITEMDELEGATE_H
#define TABLEITEMDELEGATE_H

#include <QStyledItemDelegate>

class TableItemDelegate : public QStyledItemDelegate
{
  Q_OBJECT
public:
  explicit TableItemDelegate(QObject *parent = 0);
  QWidget *createEditor(QWidget *aParent,
                        const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;
  QString displayText(const QVariant &value, const QLocale &locale) const;
  QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;
};

#endif // TABLEITEMDELEGATE_H
