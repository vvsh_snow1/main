/*
    treemodel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include <QtGui>
#include <QtDebug>

#include "treeitem.h"
#include "treemodel.h"

//! [0]
TreeModel::TreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
  QSqlQuery qry("", QSqlDatabase::database("settings"));

  qry.prepare( "SELECT * FROM tree ORDER BY tree_order, tree_caption" );
  if( !qry.exec() )
    qDebug() << qry.lastError();
  else
  {


    QList<QVariant> rootData;
    rootData << "Tree" << "Command";
    rootItem = new TreeItem(rootData);
    setupModelData(qry, rootItem);
  }
}
//! [0]

//! [1]
TreeModel::~TreeModel()
{
    delete rootItem;
}
//! [1]

//! [2]
int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}
//! [2]

//! [3]
QVariant TreeModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

  if (role == Qt::DisplayRole)
    return item->data(index.column());
  else if (role == Qt::DecorationRole) {

    if (index.column() == 0) {
      QObject* o = (QObject*)this;
      QWidget* w = (QWidget*)o->parent();
      QStyle* s = const_cast<QWidget*>(w)->style();

      if (item->childCount() > 0) {
        if (!item->data(1).toString().isEmpty())
          return s->standardIcon(QStyle::SP_DirLinkIcon);
        return s->standardIcon(QStyle::SP_DirIcon);
      }
      else {
        return s->standardIcon(QStyle::SP_FileIcon);
      }
    }
  }
  else if (role == Qt::UserRole)
    return item->data(1);

 return QVariant();
}
//! [3]

//! [4]
Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
//! [4]

//! [5]
QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}
//! [5]

//! [6]
QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}
//! [6]

//! [7]
QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}
//! [7]

//! [8]
int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}
// ----------------------------------------------------------------------
//TreeModel::TreeModel(const QSqlQuery &data, QObject *parent = 0)
//  : QAbstractItemModel(parent)
//{

//}
// ----------------------------------------------------------------------
void TreeModel::setupModelData(QSqlQuery &query, TreeItem *parent)
{
  // ��� �������� ������ ��� ���������� ������ (���������� �� tree_id)
  typedef QMap<int, TreeItem*> TreeList;
  TreeList treeList;

  // ���� ��������� ������� ���������� �� ORDER BY, � �� �� tree_id
  typedef QVector<TreeItem*> TreeVector;
  TreeVector treeVector;

  while (query.next()) {
    QList<QVariant> columnData;
    columnData << query.value(3) << query.value(2);

    TreeItem *itemCur = new TreeItem(columnData, parent);
    itemCur->Tag = query.value(1).toInt(); // parent
    treeList[query.value(0).toInt()] = itemCur;
    treeVector.append(itemCur);
  }

  // ���������� ������
  for (int i = 0; i < treeVector.count(); ++i) {
    TreeItem *itemCur = treeVector.at(i);

    if (treeList.contains(itemCur->Tag)) {
      TreeItem *itemParent = treeList[itemCur->Tag];

      itemCur->setParent(itemParent);
      itemParent->appendChild(itemCur);
    }
    else {
      parent->appendChild(itemCur);
    }
  }
/*
  // ���������� �� id, � ���� ��� ORDER BY
  for (TreeList::ConstIterator it = treeList.constBegin(); it != treeList.constEnd(); ++it) {
    TreeItem *itemCur = it.value();

    if (treeList.contains(itemCur->Tag)) {
      TreeItem *itemParent = treeList[itemCur->Tag];

      itemCur->setParent(itemParent);
      itemParent->appendChild(itemCur);
    }
    else {
      parent->appendChild(itemCur);
    }
  }
  */
}
// ----------------------------------------------------------------------
